# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 09:19:51 2020

@author: 24425
"""
import numpy as np
import copy
import random
from gradient import gradient_CD,gradient_MD,MD,gradient_equal_zero,CD

def check_U_type(x):
    n,s=np.shape(x)
    
    for i in range(s):
        a=copy.deepcopy(x[:,i])
        a.sort()
        for j in range(n):
            if a[j]<=(j+1)/n and a[j]>=j/n:
                continue
            else:
                return False
    return True
        
def CD_gradient_equal_zero_methods(n,s,best_init,lr=0.01,epoch=5000):

    init_matrix=(best_init-0.5)/n 
    
    cd_list=[]
    result=init_matrix

    cd=CD(result)
    
    for i in range(epoch):
        cd_list.append(cd)
        interm_a=gradient_equal_zero(result)

        cd = CD(interm_a)

        result=copy.deepcopy(interm_a)
        cd = CD(result)
        try:
            if cd==cd_list[-1] or cd==cd_list[-2] or not check_U_type(interm_a):
                break
        except:
            continue
        print(cd)
    return cd_list,result

def MD_gradient_methods(n,s,best_init,dtype='g',lr_list=[0.01],epoch=50000):

    init_matrix=(best_init-0.5)/n
    
    md_list=[]
    result=init_matrix
    
    md=MD(result)
    
    for lr in lr_list:
        lost_count=0
        for i in range(epoch):
            md_list.append(md)
            interm_a=result-lr*gradient_MD(result,lr,dtype=dtype)
            
            md = MD(interm_a)
            if md>md_list[-1] or not check_U_type(interm_a):
                lost_count+=1
                if lost_count>10:
                    break

            result=copy.deepcopy(interm_a)
            md = MD(result)
            print(md)
    return md_list,result




def CD_gradient_methods(n,s,best_init,dtype='g',lr_list=[0.01],epoch=50000):

    init_matrix=(best_init-0.5)/n
    
    cd_list=[]
    result=init_matrix
    
    cd=CD(result)
    
    for lr in lr_list:
        lost_count=0
        for i in range(epoch):
            cd_list.append(cd)
            interm_a=result-lr*gradient_CD(result,lr,dtype=dtype)
            
            cd = CD(interm_a)
            if cd>cd_list[-1] or not check_U_type(interm_a):
                lost_count+=1
                if lost_count>10:
                    break

            result=copy.deepcopy(interm_a)
            cd = CD(result)
            print(cd)
    return cd_list,result




