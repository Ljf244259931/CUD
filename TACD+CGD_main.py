# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 09:19:51 2020

@author: 24425
"""
import numpy as np
import random
from gradient_methods import CD as CD_without_standarization
from gradient_methods import MD as MD_without_standarization
from gradient_methods import MD_gradient_methods,check_U_type
    
def CD(x):
    x=x+1
    n,s=np.shape(x)
    q=np.max(x)
    x0=(x-0.5)/q
    return CD_without_standarization(x0)

def MD(x):
    x=x+1
    n,s=np.shape(x)
    q=np.max(x)
    x0=(x-0.5)/q
    return MD_without_standarization(x0)

def Generating(n,q,s):
    #Randomly generate a U-type design as an initialization, where x0 is of 0,1,..., q-1
    levels=list(range(q))
    cand=[val for val in levels for i in range(int(n/q))]
    x0=np.array([cand])
    for i in range(s-1):
        random.shuffle(cand)
        x0=np.insert(x0,1,values=np.array([cand]),axis=0)
    x0=x0.T
    return x0
import copy
def neigh(x):
    #Randomly generate a neighborhood of design x, 
    #that is randomly choosing one column and switch two elements
    n,s=x.shape
    xn=copy.deepcopy(x)
    col=random.sample(list(range(s)),1)
    rows=random.sample(list(range(n)),2)
    if xn[rows[0],col]==xn[rows[1],col]:
        rows=random.sample(list(range(n)),2)
    xn[rows[0],col],xn[rows[1],col]=xn[rows[1],col],xn[rows[0],col]
    return xn
    
def Tseq(n,q,s,a,I,J):
    #Retreive a T sequence for given a,I,J and size of Uniform Design.
    #a=0.1, proportion of CD function values
    #I=10, number of thresholds
    #J=5000, number of searchs
    indisc=[]
    for j in range(J):
        xc=Generating(n,q,s)
        indisc.append(CD(xc))
    T0=(max(indisc)-min(indisc))*a
    T=[]
    T.append(((I-1)/I)*T0)
    for i in range(1,I):
        T.append(((I-i-1)/I)*T[i-1])
    return T


def TACD(x0,T,I,J,N):
    #x0: initial design
    #T: threshold sequence
    #I: number of thresholds
    #J: number of searchs
    #N: number of neighborhoods found before each J search
    n,s=x0.shape
    count=0
    #Threshold Accepting algorithm
    disc=[]
    disc.append(CD(x0))
    
    x=x0
    xall=[]
    xall.append(x0)
    
    for i in range(I):
        xn=neigh(x)
        xneigh=np.array([xn])
        cdn=[]
        cdn.append(CD(xn))
        for t in range(1,N):
            xn=neigh(x)
            xneigh=np.vstack((xneigh,np.array([xn])))
            cdn.append(CD(xn))
        xn_opt_cd=min(cdn)
        loc=cdn.index(xn_opt_cd)
        
        x=xneigh[loc]
        disc.append(CD(x))
#        xall=np.vstack((xall,np.array([x])))
        xall.append(x)
        for j in range(J):
            count=count+1
#            print(count)
            xn=neigh(x)
            if CD(xn)-CD(x)<=T[i]:
                x=xn
                disc.append(CD(x))
#                xall=np.vstack((xall,np.array([x])))
                xall.append(x)

        cdnew=min(disc)
        print(cdnew)
        
    optloc=disc.index(cdnew)
    x=xall[optloc]

    print(cdnew)
    return x0,x,cdnew

def TAMD(x0,T,I,J,N):
    #x0: initial design
    #T: threshold sequence
    #I: number of thresholds
    #J: number of searchs
    #N: number of neighborhoods found before each J search
    n,s=x0.shape
    count=0
    #Threshold Accepting algorithm
    disc=[]
    disc.append(MD(x0))
    
    x=x0
    xall=[]
    xall.append(x0)
    
    for i in range(I):
        xn=neigh(x)
        xneigh=np.array([xn])
        mdn=[]
        mdn.append(MD(xn))
        for t in range(1,N):
            xn=neigh(x)
            xneigh=np.vstack((xneigh,np.array([xn])))
            mdn.append(MD(xn))
        xn_opt_md=min(mdn)
        loc=mdn.index(xn_opt_md)
        
        x=xneigh[loc]
        disc.append(MD(x))
        xall.append(x)
        for j in range(J):
            count=count+1
            xn=neigh(x)
            if MD(xn)-MD(x)<=T[i]:
                x=xn
                disc.append(MD(x))
                xall.append(x)

        mdnew=min(disc)
        print(mdnew)
        
    optloc=disc.index(mdnew)
    x=xall[optloc]

    print(mdnew)
    return x0,x,mdnew

if __name__ == '__main__':


    #TAMD
    n=27
    s=13
    x0=Generating(n,n,s)
    x=copy.deepcopy(x0)
    min_cd=100
    t=0
    for i in range(100):
        last_md=MD(x)
        I=2
        J=500
        N=1000
        T=Tseq(n,n,s,0.1,I,J)
        x0,x,mdnew=TAMD(x,T,I,J,N)
        if mdnew==last_md:
            t+=1
            print(t)
        if t>20:
            break
    print(MD(x))
    
    TAMD_CGD_md_list,TAMD_CGD_result=MD_gradient_methods(n,s,dtype='c',lr_list=[0.01],best_init=x+1)
    print(MD(TAMD_CGD_result))
    
    
#    TACD
#    n=27
#    s=13
#    x0=Generating(n,n,s)
#    x=copy.deepcopy(x0)
#    min_cd=100
#    t=0
#    for i in range(100):
#        last_cd=CD(x)
#        I=2
#        J=500
#        N=1000
#        T=Tseq(n,n,s,0.1,I,J)
#        x0,x,cdnew=TACD(x,T,I,J,N)
#        if cdnew==last_cd:
#            t+=1
#            print(t)
#        if t>20:
#            break
#    
#    print(CD(x))
#
#    from gradient_methods import CD_gradient_equal_zero_methods,CD_gradient_methods
#    #CZG
#    true_gradient_best_init_cd_list,best_matrix=CD_gradient_equal_zero_methods(n,s,best_init=x+1)
#    print(true_gradient_best_init_cd_list[-1])
#    #CGD
#    TACD_CGD_cd_list,TACD_CGD_result=CD_gradient_methods(n,s,dtype='c',lr_list=[10,1,0.1,0.01],best_init=x+1)
#    print(coordinate_true_gradient_best_init_cd_list[-1])
    
    
    #https://gitlab.com/Ljf244259931/CUD
    
    