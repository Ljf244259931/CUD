# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 17:40:10 2020

@author: 24425
"""

#encoding:utf-8
import torch
import copy
import numpy as np
def CD(a):
    n,s=np.shape(a)
    x0=copy.deepcopy(a)
    xc=abs(x0.T-0.5)

    
    mainc1=1+0.5*xc-0.5*np.multiply(xc,xc) 
#    mainc1=1+0.5*xc-0.5*xc*xc
    part2=(2/n)*np.sum(np.prod(mainc1,axis=0))
    c=np.zeros((n,1))
    xjc=abs(x0.T-0.5)
    for k in range(n):
        xk=np.ones((n,1))*np.reshape(x0[k,:],(1,len(x0[k,:])))
        xw=(abs(xk-x0)).T
        xkc=abs(xk.T-0.5)
        mainc2=1+0.5*xkc+0.5*xjc-0.5*xw
        c[k]=np.sum(np.prod(mainc2,axis=0))
    cumc=np.sum(c);
    #constant=(13/12)^s;
    #CDvar=-part2+cumc/n^2;
    CD=(13/12)**s-part2+cumc/n**2
    return CD
def MD(x):
    n,s = np.shape(x)
    x0 = copy.deepcopy(x)
    x1=abs(x0.T-0.5)
    
    main1=5/3-0.25*x1-0.25*np.multiply(x1,x1)
    part1=(2/n)*np.sum(np.prod(main1,axis=0))
    part=np.zeros((n,1))
    for i in range(n):
        xi=np.dot(np.ones((n,1)),np.reshape(x0[i,:],(1,len(x0[i,:]))))
        xi1=abs(xi.T-0.5)
        xm=abs(xi.T-x0.T)
        main2=15/8-0.25*xi1-0.25*x1-0.75*xm+0.5*np.multiply(xm,xm)
        part[i]=np.sum(np.prod(main2,axis=0))
    part2=np.sum(part);
    #constant=(19/12)^s;
    #MDvar=-part1+part2/(n^2);
    MD=(19/12)**s-part1+part2/(n**2)
    return MD

def sgn(x):
    if x>0:
        y=1
    elif x<0:
        y=-1
    else:
        y=0
    return y

def gradient_MD_lm(a,n,s,l,m):
    part1=1
    for j in range(s):
        if j!=m:
            part1=part1*(5/3-0.25*abs(a[l,j]-0.5)-0.25*(a[l,j]-0.5)**2)
        else:
            part1=part1*(-0.25*sgn(a[l,m]-0.5)-0.5*(a[l,m]-0.5))
    part2=0
    for k in range(n):
        sub_part2=1
        if k==l:
            continue
        for j in range(s):
            if j!=m:
                sub_part2=sub_part2*(15/8-0.25*abs(a[l,j]-0.5)-0.25*abs(a[k,j]-0.5)-0.75*abs(a[l,j]-a[k,j])+0.5*(a[l,j]-a[k,j])**2)
            else:
                sub_part2=sub_part2*(-0.25*sgn(a[l,m]-0.5)-0.75*sgn(a[l,m]-a[k,m])+(a[l,m]-a[k,m]))
        part2+=2*sub_part2
    sub_part2=1
    for j in range(s):
        if j!=m:
            sub_part2=sub_part2*(15/8-0.5*abs(a[l,j]-0.5))
        else:
            sub_part2=sub_part2*(-0.5*sgn(a[l,m]-0.5))
    part2+=sub_part2
    
    return -2/n*part1 + 1/n**2*part2

def gradient_MD(a,lr,dtype='c'):
    n,s=np.shape(a)
    cd_a=CD(a)
    
    gradient_matrix=np.zeros((n,s))
    min_cd=cd_a
    for l in range(n):
        for m in range(s):
            if dtype=='c':
                a_copy=copy.deepcopy(a)
                coordinate_g=gradient_MD_lm(a,n,s,l,m)
                a_copy[l,m]=a_copy[l,m]-lr*coordinate_g
                if CD(a_copy)<min_cd:
                    
                    min_cd=CD(a_copy)
#                    print('success',coordinate_g)
                    gradient_matrix=np.zeros((n,s))
                    gradient_matrix[l,m]=coordinate_g
            else:
                gradient_matrix[l,m]=gradient_MD_lm(a,n,s,l,m)
    return gradient_matrix





def gradient_CD_lm(a,n,s,l,m):
    part1=1
    for j in range(s):
        if j!=m:
            part1=part1*(1+0.5*abs(a[l,j]-0.5)-0.5*(a[l,j]-0.5)**2)
        else:
            part1=part1*(0.5*sgn(a[l,m]-0.5)-(a[l,m]-0.5))
    part2=0
    for k in range(n):
        sub_part2=1
        if k==l:
            continue
        for j in range(s):
            if j!=m:
                sub_part2=sub_part2*(1+0.5*abs(a[l,j]-0.5)+0.5*abs(a[k,j]-0.5)-0.5*abs(a[l,j]-a[k,j]))
            else:
                sub_part2=sub_part2*0.5*(sgn(a[l,m]-0.5)-sgn(a[l,m]-a[k,m]))
        part2+=2*sub_part2
    sub_part2=1
    for j in range(s):
        if j!=m:
            sub_part2=sub_part2*(1+abs(a[l,j]-0.5))
        else:
            sub_part2=sub_part2*sgn(a[l,m]-0.5)
    part2+=sub_part2
    
    return -2/n*part1 + 1/n**2*part2




def gradient_equal_zero_lm(a,n,s,l,m):
    
    denominator=1
    
    part1=1
    for j in range(s):
        if j!=m:
            part1=part1*(1+0.5*abs(a[l,j]-0.5)-0.5*(a[l,j]-0.5)**2)
            denominator=denominator*(1+0.5*abs(a[l,j]-0.5)-0.5*(a[l,j]-0.5)**2)
        else:
            part1=part1*0.5*sgn(a[l,m]-0.5)
    part2=0
    for k in range(n):
        sub_part2=1
        if k==l:
            continue
        for j in range(s):
            if j!=m:
                sub_part2=sub_part2*(1+0.5*abs(a[l,j]-0.5)+0.5*abs(a[k,j]-0.5)-0.5*abs(a[l,j]-a[k,j]))
            else:
                sub_part2=sub_part2*0.5*(sgn(a[l,m]-0.5)-sgn(a[l,m]-a[k,m]))
        part2+=2*sub_part2
    sub_part2=1
    for j in range(s):
        if j!=m:
            sub_part2=sub_part2*(1+abs(a[l,j]-0.5))
        else:
            sub_part2=sub_part2*sgn(a[l,m]-0.5)
    part2+=sub_part2
    
    numerator = -2/n*part1 + 1/n**2*part2
    denominator = -denominator*2/n
    
    return numerator/denominator+0.5











def gradient_equal_zero(a):
    n,s=np.shape(a)
    for l in range(n):
        for m in range(s):
            a[l,m] = gradient_equal_zero_lm(a,n,s,l,m)
    return a


'''
dtype='g' -> gradient descent
dtype='c' -> coordinate gradient descent
'''
def gradient_CD(a,lr,dtype='c'):
    n,s=np.shape(a)
    cd_a=CD(a)
    
    gradient_matrix=np.zeros((n,s))
    min_cd=cd_a
    for l in range(n):
        for m in range(s):
            if dtype=='c':
                a_copy=copy.deepcopy(a)
                coordinate_g=gradient_CD_lm(a,n,s,l,m)
                a_copy[l,m]=a_copy[l,m]-lr*coordinate_g
                if CD(a_copy)<min_cd:
                    
                    min_cd=CD(a_copy)
#                    print('success',coordinate_g)
                    gradient_matrix=np.zeros((n,s))
                    gradient_matrix[l,m]=coordinate_g
            else:
                gradient_matrix[l,m]=gradient_CD_lm(a,n,s,l,m)
    return gradient_matrix